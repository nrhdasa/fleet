from frappe import _

def get_data():
	return [
		{
			"module_name": "Fleet Management",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Fleet Management")
		}
	]
